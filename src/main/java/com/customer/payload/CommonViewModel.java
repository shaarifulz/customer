package com.customer.payload;

public class CommonViewModel {

	String id;
	String customerId;
	String productId;
	String orderId;
	String username;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	public String getProductId() {
		return productId;
	}
	public void setProductId(String productId) {
		this.productId = productId;
	}
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public CommonViewModel(String id, String customerId, String productId, String orderId, String username) {
		this.id = id;
		this.customerId = customerId;
		this.productId = productId;
		this.orderId = orderId;
		this.username = username;
	}
	public CommonViewModel() {
	}
	
	
}

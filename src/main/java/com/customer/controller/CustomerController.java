package com.customer.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.customer.payload.CommonViewModel;
import com.customer.service.CustomerService;

import jakarta.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/customer")
public class CustomerController {

	@Autowired
	CustomerService customerService;
	
	
	@PostMapping("/save-customer")
	public ResponseEntity<Map<String, Object>> saveCustomer(HttpServletRequest request, @RequestBody CommonViewModel customer){
		
		Map<String, Object> data = customerService.saveCustomer(customer);
		ResponseEntity<Map<String,Object>> responseEntity = null;
		
		responseEntity = new ResponseEntity<Map<String,Object>>(data, HttpStatus.OK);	
		return responseEntity;
	}
	
	@PostMapping("/get-customers")
	public ResponseEntity<Map<String, Object>> getCustomers(HttpServletRequest request){
		
		Map<String, Object> data = customerService.getCustomers();
		ResponseEntity<Map<String,Object>> responseEntity = null;
		
		responseEntity = new ResponseEntity<Map<String,Object>>(data, HttpStatus.OK);	
		return responseEntity;
	}
	
	//@PostMapping("/get-customer-by-id")
	public ResponseEntity<Map<String, Object>> getCustomerById(HttpServletRequest request, @RequestBody CommonViewModel viewModel){
		
		Map<String, Object> data = customerService.getCustomerById(viewModel);
		ResponseEntity<Map<String,Object>> responseEntity = null;
		
		responseEntity = new ResponseEntity<Map<String,Object>>(data, HttpStatus.OK);	
		return responseEntity;
	}
	
	
	@GetMapping("/get-customer-by-id/{id}")
	public ResponseEntity<CommonViewModel> getCustomerById2(HttpServletRequest request, @PathVariable("id") Long id){
		
		ResponseEntity<CommonViewModel> responseEntity = null;
		CommonViewModel response = new CommonViewModel("id","customerId","productId","orderId","username");

		responseEntity = new ResponseEntity<CommonViewModel>(response, HttpStatus.OK);	

		return responseEntity;
	}
	
}

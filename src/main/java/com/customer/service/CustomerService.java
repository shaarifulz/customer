package com.customer.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.customer.payload.CommonViewModel;

@Service
public class CustomerService{
	
	public Map<String, Object> saveCustomer(CommonViewModel customer) {
		Map<String, Object> data = new HashMap<String, Object>();
		
		CommonViewModel commonViewModel = new CommonViewModel();
		data.put("data", commonViewModel);
		return data;
	}

	public Map<String, Object> getCustomers() {
		Map<String, Object> data = new HashMap<String, Object>();
		
		List<CommonViewModel> custList = new ArrayList<>();
		data.put("data", custList);
		return data;
	}

	public Map<String, Object> getCustomerById(CommonViewModel viewModel) {
		Map<String, Object> data = new HashMap<String, Object>();
		
		CommonViewModel commonViewModel = new CommonViewModel("101", "customerId", "3001", "2001", "username");

		if (commonViewModel.getId()!=null) {
			data.put("data", commonViewModel);
			data.put("status", "successful");
		}else {
			data.put("data", new ArrayList<>());
			data.put("status", "failure");
		}
		return data;
	}

}
